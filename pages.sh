#!/usr/bin/env bash
set -euo pipefail

site_dir="site"
main_dir=""
pages_dir="public"
template="index.html"
links=""

mkdir -p "${pages_dir}"

# Set the diff tool
git config diff.pandoc.textconv 'pandoc -t markdown'

# Get the relevant commits from the log
commits=$(git log --reverse --pretty=oneline | grep "Update at" | cut -d' ' -f1)

# Create the website for the relevant commit
for c in $commits
do
    # Checkout the relevant commit
    git checkout "$c" -- "${site_dir}"

    # Get the date of the commit
    date=$(git show -s --format=%ai "$c")

    # Show the latest commit
    git diff "$c~1" "$c" -- "${site_dir}" > "${pages_dir}/${c}.diff.txt"

    # Check if commit is empty
    if [[ -s "${pages_dir}/${c}.diff.txt" ]]
    then
        diff="<a href=\"${c}.diff.txt\">diff</a>"
    else
        diff="no changes"
    fi

    # Copy the state of the site to the pages dir
    cp -a "${site_dir}" "${pages_dir}/${date}"

    # Add a link to the index
    links="${links}<li><a href=\"${date}/${main_dir}\">${date}</a> (${diff})</li>"

    # Create a 'latest' symlink for the latest commit
    [ ! -e "${pages_dir}/latest" ] && ln -s "${date}" "${pages_dir}/latest"
done

# Create index file
sed "s|{{body}}|${links}|g" "${template}" > "${pages_dir}/index.html"
