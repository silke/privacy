#!/usr/bin/env bash
set -euo pipefail

cd $(dirname "$0")

URL="https://www.utwente.nl/en/educational-systems/about_the_applications/canvas/policies/privacy-statement/"
SITE_DIR=site

# Retrieve website
wget -q -O "${SITE_DIR}/index.html" "${URL}"

# Add to Git
git add site
git commit -m "Update at $(date '+%F %T')" >/dev/null || exit 0
git push
